# RT-Thread
该项目用于存放 RT-Thread书签

官方资料
==

[官网](https://www.rt-thread.org/):https://www.rt-thread.org/

[文档首页](https://www.rt-thread.org/document/site/):https://www.rt-thread.org/document/site/

[SCons 构建工具](https://www.rt-thread.org/document/site/programming-manual/scons/scons/):https://www.rt-thread.org/document/site/programming-manual/scons/scons/

[Keil 模拟器 STM32F103 上手指南](https://www.rt-thread.org/document/site/tutorial/quick-start/stm32f103-simulator/stm32f103-simulator/):https://www.rt-thread.org/document/site/tutorial/quick-start/stm32f103-simulator/stm32f103-simulator/

[STM32 BSP 说明](https://github.com/RT-Thread/rt-thread/blob/master/bsp/stm32/README.md):https://github.com/RT-Thread/rt-thread/blob/master/bsp/stm32/README.md

[新手推荐开发板列表及其 BSP](https://www.rt-thread.org/document/site/tutorial/quick-start/more/):https://www.rt-thread.org/document/site/tutorial/quick-start/more/

