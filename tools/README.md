
Shell
==

## Windows
[Babun](http://babun.github.io ): [github](https://github.com/babun/babun) [Download](http://projects.reficio.org/babun/download)

[Babun，一个开箱即用的 Windows Shell](https://blog.csdn.net/ly0303521/article/details/77960463):https://blog.csdn.net/ly0303521/article/details/77960463

### Compiler tool
[ARM 编译工具keil 和 IAR 命令行编译和下载](https://www.cnblogs.com/memorypro/p/9562919.html):https://www.cnblogs.com/memorypro/p/9562919.html


## Editor
[Notepad++](https://notepad-plus-plus.org/):https://notepad-plus-plus.org/

[Visual Studio Code](https://code.visualstudio.com/):https://code.visualstudio.com/

[Atom](https://atom.io/):https://atom.io/

[Source insight(https://www.sourceinsight.com/):https://www.sourceinsight.com/]

## Qt
[Qt Downloads](http://download.qt.io/):http://download.qt.io/

[Qt Creator的下载和安装](https://blog.csdn.net/weixin_38090427/article/details/83827678):https://blog.csdn.net/weixin_38090427/article/details/83827678

# IDEA 
[IDEA](http://www.jetbrains.com/idea/):http://www.jetbrains.com/idea/

[IDEA](http://www.jetbrains.com/):http://www.jetbrains.com/


## 思维导图
[XMind](https://www.xmind.net/):https://www.xmind.net/

[FreeMind](https://freemind.softonic.cn/):https://freemind.softonic.cn/

[edrawsoft](https://mm.edrawsoft.cn):https://mm.edrawsoft.cn

[mindline](http://www.mindline.cn):http://www.mindline.cn

















